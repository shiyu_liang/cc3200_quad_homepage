var populateCommits = function(xml, input_div, max) {
    $.each($(xml).find('item'), function (i, el) {
        var commit_url = $(el).find('link').html();
        var commit_id = commit_url.split('/');
        var date = new Date($(el).find('pubDate').html());

        var li = $('<li/>')
            .addClass('commit-list')
            .appendTo(input_div);
        var commit_header = $('<div/>')
            .addClass('commit-header')
            .appendTo(li);
        var commit = $('<a/>')
            .addClass('commit-link')
            .attr('href', commit_url)
            .attr('target', '_blank')
            .text(commit_id.pop())
            .appendTo(commit_header);
            li.append(' by ' + $(el).find('author').html());
            li.append(' on ' + date.toLocaleString());
        console.log();
        var title = $('<div/>')
            .addClass('commit-body')
            .text($(el).find('title').html())
            .appendTo(li);
        if (i + 1 >= max) {
            return false;
        }
    });
}

var populateRssDivWithUrl = function(input_div, input_url) {
    $(input_div).ready(function(){
        var ret_data = $.ajax({
            type: 'GET',
            url: input_url,
            dataType: "xml",
            success: function(xml) {
                $(input_div).append('<ul>');

                populateCommits(xml, input_div, 10);

                $(input_div).append('</ul>');
            },
            error: function() {
                alert('couldn\'t get from ' + input_url);
            }
        })
    });
}
