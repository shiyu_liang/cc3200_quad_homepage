$(document).ready(function () {
    populateRssDivWithUrl('#divRssAndroid', 'https://bitbucket.org/shiyu_liang/cc3200_quad_android2/rss');
    populateRssDivWithUrl('#divRssHW', 'https://bitbucket.org/shiyu_liang/cc3200_quad_hardware/rss');
    populateRssDivWithUrl('#divRssSW', 'https://bitbucket.org/shiyu_liang/cc3200_quad_firmware_gnu/rss');
    populateRssDivWithUrl('#divRssHome', 'https://bitbucket.org/shiyu_liang/cc3200_quad_homepage/rss');

    $('#tabs').tabs();

    $('#tabs.ui-tabs').tabs({
        select: function(event, ui){
        // Do stuff here
            console.log('select');
        },
        activate: function(event ,ui){
            var target = event.currentTarget.innerHTML;
            if (target == 'Gallery') {
                $('#nanogallery').nanoGallery({
                    kind: 'picasa',
                    userID: '115843252336330602131'
                });
            }
        }
    });
});
